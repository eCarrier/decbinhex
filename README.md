# DECBINHEX

## This project

decbinhex is a small and simple C utility used to easily print multiple numbers in the binary, decimal and (or) hexadecimal format.

I've made because I was tired of going online for simple conversions and
thought a simple CLI tool would be nice

## Platform

I've only tested it on arch linux 5.7.4 but should work on almost any C
compiler as it is C11 compliant

## Installation 

On linux, if you have GCC and make installed, simply do :

```sh

make all
sudo make install

```

## Usage

After installing, simply enter the number as an argument :

```sh
$ decbinhex 123 0x1ff2 0b1100                                                                                                                                                            
Decimal :       123
Hexadecimal :   0x7b
Binary :        0b1111011   
-------------------------------                                                                                                                                                                                     
Decimal :       8178
Hexadecimal :   0x1ff2
Binary :        0b1111111110010
-------------------------------

Decimal :       12
Hexadecimal :   0xc
Binary :        0b1100                                                                                                                                                                                        
-------------------------------    
```


## Things that I plan to do :

* Add tests
* Add support for numbers larger than what normally fits in a 8 bytes integer



