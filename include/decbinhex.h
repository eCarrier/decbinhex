#ifndef DECBINHEX_H
#define DECBINHEX_H

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <errno.h>
#include <ctype.h>

// Flags management
#define FLAG_HELP            1
#define FLAG_INTERACTIVE     2
#define FLAG_ONLY_BINARY     4
#define FLAG_ONLY_DECIMAL    8
#define FLAG_ONLY_HEXA       16
#define FLAG_VERBOSE         32

//Macros

// After having some issues with displaying negative number with a recursive
// left bitshift solution, i've decided to go for a macro solution detailed
// here, for negative numbers only : 
// See ideasman response
// https://stackoverflow.com/questions/111928/is-there-a-printf-converter-to-print-in-binary-format
// /* --- PRINTF_BYTE_TO_BINARY macro's --- */
#define PRINTF_BINARY_PATTERN_INT8 "%c%c%c%c%c%c%c%c"
#define PRINTF_BYTE_TO_BINARY_INT8(i)    \
    (((i) & 0x80ll) ? '1' : '0'), \
    (((i) & 0x40ll) ? '1' : '0'), \
    (((i) & 0x20ll) ? '1' : '0'), \
    (((i) & 0x10ll) ? '1' : '0'), \
    (((i) & 0x08ll) ? '1' : '0'), \
    (((i) & 0x04ll) ? '1' : '0'), \
    (((i) & 0x02ll) ? '1' : '0'), \
    (((i) & 0x01ll) ? '1' : '0')

#define PRINTF_BINARY_PATTERN_INT16 \
    PRINTF_BINARY_PATTERN_INT8              PRINTF_BINARY_PATTERN_INT8
#define PRINTF_BYTE_TO_BINARY_INT16(i) \
    PRINTF_BYTE_TO_BINARY_INT8((i) >> 8),   PRINTF_BYTE_TO_BINARY_INT8(i)
#define PRINTF_BINARY_PATTERN_INT32 \
    PRINTF_BINARY_PATTERN_INT16             PRINTF_BINARY_PATTERN_INT16
#define PRINTF_BYTE_TO_BINARY_INT32(i) \
    PRINTF_BYTE_TO_BINARY_INT16((i) >> 16), PRINTF_BYTE_TO_BINARY_INT16(i)
#define PRINTF_BINARY_PATTERN_INT64    \
    PRINTF_BINARY_PATTERN_INT32             PRINTF_BINARY_PATTERN_INT32
#define PRINTF_BYTE_TO_BINARY_INT64(i) \
    PRINTF_BYTE_TO_BINARY_INT32((i) >> 32), PRINTF_BYTE_TO_BINARY_INT32(i)


const char *SHORT_FLAGS_CHAR[]  = {"-h", "-i", "-b", "-d", "-x", "-v"};
const int MAX_FLAGS             = 6;


// Error messages
const char ERR_NOT_ENOUGH_ARG[]     = "Error : at least one number should be input";
const char ERR_FOLL_INVALID_INPUT[] = "Error : The following input is invalid :"; 
const char ERR_INVALID_INPUT[]      = "Error : Invalid input:"; 
const char USAGE[]                  = "Usage : decbinhex [OPTION]... [NUMBERS]...\nEnter a list of numbers to get their conversions in decimal, binary and hexadecimal\nEnter -h to see the different options.";
// Other messages
const char HELP[]                   = "-i To use interactive mode\n-b Show only binary\n-d Show only decimal\n-x Show only hexadecimal\n-v verbose mode\nHexadecimal input must start with 0x and binary by 0b";

/**
 * Function to parse the command line arguments
 *
 * This function parses the arguments in the command line using the typical
 * argc and args.
 *
 * @param argc is the number of arguments
 * @param args is the arguments
 * @param flags are the flags entered by the user
 * @param is an array for the numbers
 *
 * @return 1 on success, 0 if the parsing didn't work
 */
int parse_args      (int argc, char *args[], uint16_t *flags, char *numbers[]);
/**
 * Main function processing user input
 * 
 * @param flags are the flags entered by the user
 * @param is an array for the numbers
 * 
 */
void print_number   (char number[], uint16_t flags);
/**
 * Tries to process a decimal number that is stored in a string
 *
 * @param number is a decimal number in a string
 * @param  where the actual value will be stored
 *
 * return 1 if the number was successfully stored in the pointer, 0 if didn't
 */
int process_decimal (char number[], int64_t *p_val);
/**
 * Tries to process a binary number that is stored in a string
 *
 * @param number is a binary number in a string
 * @param  where the actual value will be stored on success
 *
 * return 1 if the number was successfully stored in the pointer, 0 if didn't
 */
int process_binary  (char number[], int64_t *p_val);
/**
 * Tries to process a hexadecimal number that is stored in a string
 *
 * @param number is a hexadecimal number in a string
 * @param  where the actual value will be stored on success
 *
 * return 1 if the number was successfully stored in the pointer, 0 if didn't
 */
int process_hexa    (char number[], int64_t *p_val);

/**
 * This function will print a binary number
 */
void print_binary   (int64_t number);

// Extern variables
extern int errno ;

#endif
