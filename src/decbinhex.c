#include "../include/decbinhex.h"

// See the header file for function descriptions

int main(int argc, char *args[])
{
  uint16_t  flags         = 0;
  int       flags_count   = 0;
  char      user_number[100];
  char      *numbers[argc];

  // Exit when there is not enough arguments
  if (argc == 1)
  {
    printf("%s\n\n%s\n", ERR_NOT_ENOUGH_ARG, USAGE);
    return 1;
  }
  else 
  {
    // You'll need to exit when the program is not in interactive mode and // there's only flags entered
    flags_count = parse_args(argc, args, &flags, numbers) ;
    // If you didn't enter any number and you're not either asking for help or
    // the interactive mode, exit
    if ( flags_count == argc-1 && !( ( flags & FLAG_INTERACTIVE ) == FLAG_INTERACTIVE)  && !( ( flags & FLAG_HELP ) == FLAG_HELP ) )
    {
      printf("%s\n\n%s\n", ERR_NOT_ENOUGH_ARG, USAGE);
      return 1;
    }
    else 
    {
      // Show the help informations
      if ( ( flags & FLAG_HELP ) == FLAG_HELP ) 
      {
        printf("%s\n", HELP);
      }
      // Deal with numbers
      for ( int i = 0 ; i < argc - 1 - flags_count ; ++i )
        print_number(numbers[i], flags);
      // Interactive mode : here you input numbers one after another until you
      // CTRL-C
      if ( (flags & FLAG_INTERACTIVE) == FLAG_INTERACTIVE )
      { 
        while (1)
        {
          if ( scanf("%s", user_number) == 1 )
          {
            print_number(user_number, flags);
          }
        }
      }
    }
  }
  return 0;
}


int parse_args(int argc, char *args[], uint16_t *flags, char *numbers[])  
{
  // Used to go throught short flags array
  int y = 0;
  int parsed_flags_count = 0;
  int current_number_index = 0;
  // Go throught the arguments 
  // Skip the first one
  for( int i = 1; i < argc; ++i ) 
  {
    // We look for args starting with -
    if (args[i][0] == '-' && !isdigit(args[i][1]))
    {
      // Go throught the known flags
      for ( y = 0;  y < MAX_FLAGS; ++y )
      {
        // Compare the flag to one of the known flag 
        if ( strcmp(args[i], SHORT_FLAGS_CHAR[y]) == 0 )
        {
          *flags |= (int) pow(2, y);
          break;
        }
      }
      ++parsed_flags_count;
      if ( y == MAX_FLAGS )
      {
        printf("%s is an unknown flag\n", args[i]);
      }
    }
    // If it is not a flag, it is assumed to be a number
    else
      numbers[current_number_index++] = args[i];
  } 
  return parsed_flags_count;
}


void print_number(char number[], uint16_t flags)
{
  // Variable for the user input
  int64_t val = 0;
  // Success flag
  int success = 0;
  if ( ( flags & FLAG_VERBOSE ) == FLAG_VERBOSE )
    printf("Input is : %s\n===================\n", number);
  // Tests for hexadecimal input
  if ( number[0] == '0' && number[1] == 'x' )
    success = process_hexa(number, &val);
  // Tests for binary input
  else if ( number[0] == '0' && number[1] == 'b' )
    success = process_binary(number, &val);
  // Else its treated as a decimal input
  else
    success = process_decimal(number, &val);
  // If the number was successfully parsed
  if (success)
  {
    // Here we're going to print accordingly to the user flags
    if ( ( flags & FLAG_ONLY_HEXA ) == FLAG_ONLY_HEXA )
      printf("0x%lx\n", val);
    if ( ( flags & FLAG_ONLY_DECIMAL ) == FLAG_ONLY_DECIMAL )
      printf("%ld\n", val);
    if ( ( flags & FLAG_ONLY_BINARY ) == FLAG_ONLY_BINARY )
    { 
      printf("0b");
      print_binary(val);
      printf("\n");
    }
    // Print every format if there was no flag specified
    if ( !((flags & FLAG_ONLY_HEXA) == FLAG_ONLY_HEXA ||  (flags & FLAG_ONLY_DECIMAL) == FLAG_ONLY_DECIMAL || (flags & FLAG_ONLY_BINARY) == FLAG_ONLY_BINARY) )
    {
      printf("Decimal : \t%ld\n", val);
      printf("Hexadecimal : \t0x%lx\n", val);
      printf("Binary : \t0b");
      print_binary(val);
      printf("\n");
      printf("-------------------------------\n\n");
    }
  }
}

int process_decimal(char number[], int64_t *p_val)
{
  int success = 1;
  char *p_end;
  // We try to put the number in a variable
  int64_t val=strtol(number, &p_end, 10);
  *p_val = val;
  if ( errno == ERANGE || *p_end != 0 )
  {
    printf("%s%s\n", ERR_INVALID_INPUT, number);
    success = 0;
  }
  return success;
}

int process_binary(char number[], int64_t *p_val)
{
  int success = 1;
  char *p_end;
  number[0]='0';
  number[1]='0';
  // We try to put the number in a variable
  int64_t val=strtol(number, &p_end, 2);
  *p_val = val;
  number[0]='0';
  number[1]='b';
  if ( errno == ERANGE || *p_end != 0 )
  {
    printf("%s%s\n", ERR_INVALID_INPUT, number);
    success = 0;
  }
  return success;
}

int process_hexa(char number[], int64_t *p_val)
{
  int success = 1;
  char *p_end;
  // We try to put the number in a variable
  int64_t val=strtol(number, &p_end, 16);
  *p_val = val;
  if ( errno == ERANGE || *p_end != 0 )
  {
    printf("%s%s\n", ERR_INVALID_INPUT, number);
    success = 0;
  }
  return success;
}

// See danija's response
// https://stackoverflow.com/questions/111928/is-there-a-printf-converter-to-print-in-binary-format
void print_binary(int64_t number)
{
  if ( number < 0 )
  {
    printf(PRINTF_BINARY_PATTERN_INT64 "\n",
           PRINTF_BYTE_TO_BINARY_INT64(number));
  }
  else if (number)
  {
    print_binary(number >> 1);
    putc((number & 1) ? '1' : '0', stdout);
  } 
}


