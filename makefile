# ==================================================
#                Makefile for decbinhex
# ==================================================
#
# =================================================
#                     Variables
# =================================================
# Folders
INSTALL_DIR 	= /usr/local/bin
SRC_DIR 			= src
INCLUDE_DIR 	= include
BIN_DIR 			= bin
BUILD_DIR 		= .build
OBJ_DIR 			= $(BUILD_DIR)/obj
DIR_TO_CREATE = $(BIN_DIR) $(BUILD_DIR) $(OBJ_DIR)

# The main thing
EXE 					= $(BIN_DIR)/decbinhex

# The relevant C files
_OBJS 				= decbinhex.o
_DEPS 				= decbinhex.h

# Adding the folder destination to everything
DEPS 					= $(patsubst %,$(INCLUDE_DIR)/%,$(_DEPS))
OBJS 					= $(patsubst %,$(OBJ_DIR)/%,$(_OBJS))

# GCC stuff
CC 						= gcc
LIBS 					= -lm
CFLAGS 				= -g -Wall

# =================================================
#                   Targets	
# =================================================

.PHONY: clean all install

all: $(DIR_TO_CREATE) $(EXE) 

# Create the executable
$(EXE) : $(OBJS)
	$(CC) $(CFLAGS) $(INCLUDES) -o $@ $^ $(LIBS)

# Create the object files
$(OBJ_DIR)/%.o : $(SRC_DIR)/%.c $(DEPS)
	$(CC) -c $< $(CFLAGS) -I $(INCLUDE_DIR) -o $@ 

# Create the various building directories 
$(DIR_TO_CREATE) :
	mkdir -p $@
	
# Install t
install:
	cp $(EXE) $(INSTALL_DIR)

clean:
	rm -r -f $(DIR_TO_CREATE)
	rm -f decbinhex
